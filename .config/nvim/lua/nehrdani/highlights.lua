local set = vim.opt

set.cursorline = true
set.termguicolors = true
set.fillchars:append { diff = '/' }
