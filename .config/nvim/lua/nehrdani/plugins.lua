local status, packer = pcall(require, 'packer')
if not status then
  print('Packer is not installed')
  return
end

vim.cmd [[packadd packer.nvim]]

packer.startup(function(use)
  use 'wbthomason/packer.nvim'
  use 'shaunsingh/nord.nvim'
  use 'goolord/alpha-nvim'
  -- Statusline
  use 'nvim-lualine/lualine.nvim'

  use { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    requires = {
      -- Automatically install LSPs to stdpath for neovim
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      'j-hui/fidget.nvim',
    }
  }
  -- Better typescript support
  use 'jose-elias-alvarez/typescript.nvim'
  -- Use Neovim as a language server to inject LSP diagnostics, code actions, and more via Lua
  use 'jose-elias-alvarez/null-ls.nvim'

  use { -- Autocompletion
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip'
    },
  }

  -- vscode-like pictograms
  use 'onsails/lspkind-nvim'

  use { -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    run = function()
      pcall(require('nvim-treesitter.install').update { with_sync = true })
    end,
  }

  use { -- Additional text objects via treesitter
    'nvim-treesitter/nvim-treesitter-textobjects',
    after = 'nvim-treesitter',
  }

  -- Fuzzy Finder (files, lsp, etc)
  use { 'nvim-telescope/telescope.nvim', branch = '0.1.x', requires = { 'nvim-lua/plenary.nvim' } }
  -- Fuzzy Finder Algorithm which requires local dependencies to be built.
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
  -- Fuzzy Finder extension for browsing
  use 'nvim-telescope/telescope-file-browser.nvim'

  use { "glepnir/lspsaga.nvim", branch = "main" } -- LSP UIs
  use 'kyazdani42/nvim-web-devicons' -- File icons
  use 'windwp/nvim-autopairs'
  use 'windwp/nvim-ts-autotag'
  use 'numToStr/Comment.nvim'
  use 'kylechui/nvim-surround'
  use 'NvChad/nvim-colorizer.lua'
  use 'sindrets/diffview.nvim'
  use 'pwntester/octo.nvim'
  use 'lewis6991/gitsigns.nvim'
end)
