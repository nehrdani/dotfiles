local set = vim.opt

set.number = true
set.signcolumn = 'yes'
set.showmode = false
set.backup = false
set.swapfile = false
set.tabstop = 2
set.shiftwidth = 2
set.expandtab = true
set.smartindent = true
set.autoindent = true
set.breakindent = true
set.ignorecase = true
set.smartcase = true
set.scrolloff = 8
set.splitright = true
set.splitbelow = true
set.wrap = false
set.list = true
set.listchars = { tab = '»·', trail = '·' }
set.path:append { '**' } -- Finding files - Search down into subfolders
set.wildignore:append { '*/node_modules/*' }
set.shell = 'zsh'
set.clipboard:append { 'unnamedplus' }

vim.o.updatetime = 250 -- For `hover` actions
