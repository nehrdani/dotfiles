local status, alpha = pcall(require, "alpha")
if not status then return end

local path_ok, plenary_path = pcall(require, "plenary.path")
if not path_ok then return end

local dashboard = require('alpha.themes.dashboard')

local nvim_web_devicons = {
  enabled = true,
  highlight = true,
}

local function get_extension(fn)
  local match = fn:match("^.+(%..+)$")
  local ext = ""
  if match ~= nil then
    ext = match:sub(2)
  end
  return ext
end

local function icon(fn)
  local devicons = require("nvim-web-devicons")
  local ext = get_extension(fn)
  return devicons.get_icon(fn, ext, { default = true })
end

local function file_button(fn, sc, short_fn, autocd)
  short_fn = short_fn or fn
  local ico_txt
  local fb_hl = {}

  if nvim_web_devicons.enabled then
    local ico, hl = icon(fn)
    local hl_option_type = type(nvim_web_devicons.highlight)
    if hl_option_type == "boolean" then
      if hl and nvim_web_devicons.highlight then
        table.insert(fb_hl, { hl, 0, 3 })
      end
    end
    if hl_option_type == "string" then
      table.insert(fb_hl, { nvim_web_devicons.highlight, 0, 3 })
    end
    ico_txt = ico .. "  "
  else
    ico_txt = ""
  end
  local cd_cmd = (autocd and " | cd %:p:h" or "")
  local file_button_el = dashboard.button(sc, ico_txt .. short_fn, "<cmd>e " .. fn .. cd_cmd .. " <CR>")
  local fn_start = short_fn:match(".*[/\\]")
  if fn_start ~= nil then
    table.insert(fb_hl, { "Comment", #ico_txt - 2, #fn_start + #ico_txt })
  end
  file_button_el.opts.hl = fb_hl
  return file_button_el
end

local default_mru_ignore = { "gitcommit" }

local mru_opts = {
  ignore = function(path, ext)
    return (string.find(path, "COMMIT_EDITMSG")) or (vim.tbl_contains(default_mru_ignore, ext))
  end,
}

--- @param start number
--- @param cwd string optional
--- @param items_number number optional number of items to generate, default = 10
local function mru(start, cwd, items_number, opts)
  opts = opts or mru_opts
  items_number = vim.F.if_nil(items_number, 11)

  local oldfiles = {}
  for _, v in pairs(vim.v.oldfiles) do
    if #oldfiles == items_number then
      break
    end
    local cwd_cond
    if not cwd then
      cwd_cond = true
    else
      cwd_cond = vim.startswith(v, cwd)
    end
    local ignore = (opts.ignore and opts.ignore(v, get_extension(v))) or false
    if (vim.fn.filereadable(v) == 1) and cwd_cond and not ignore then
      oldfiles[#oldfiles + 1] = v
    end
  end
  local target_width = 39

  local tbl = {}
  for i, fn in ipairs(oldfiles) do
    local short_fn
    if cwd then
      short_fn = vim.fn.fnamemodify(fn, ":.")
    else
      short_fn = vim.fn.fnamemodify(fn, ":~")
    end

    if #short_fn > target_width then
      short_fn = plenary_path.new(short_fn):shorten(1, { -2, -1 })
      if #short_fn > target_width then
        short_fn = plenary_path.new(short_fn):shorten(1, { -1 })
      end
    end

    local shortcut = tostring(i + start - 1)

    local file_button_el = file_button(fn, shortcut, short_fn)
    tbl[i] = file_button_el
  end
  return {
    type = "group",
    val = tbl,
    opts = {},
  }
end

local function surround(v)
  return ' ' .. v .. ' '
end

local function info_value()
  local devicons = require('nvim-web-devicons')
  local total_plugins = #vim.tbl_keys(packer_plugins)
  local datetime = os.date(surround('') .. '%d-%m-%Y')
  local version = vim.version()
  local nvim_version_info = surround(devicons.get_icon_by_filetype('vim', {}))
      .. 'v'
      .. version.major
      .. '.'
      .. version.minor
      .. '.'
      .. version.patch

  return datetime .. surround('') .. total_plugins .. ' plugins' .. nvim_version_info
end

local logo = {
  type = 'text',
  val = {
    [[88888b.   .d88b.   .d88b.  888  888 888 88888b.d88b. ]],
    [[888 "88b d8P  Y8b d88""88b 888  888 888 888 "888 "88b]],
    [[888  888 88888888 888  888 Y88  88P 888 888  888  888]],
    [[888  888 Y8b.     Y88..88P  Y8bd8P  888 888  888  888]],
    [[888  888  "Y8888   "Y88P"    Y88P   888 888  888  888]],
    -- [[                               __                ]],
    -- [[  ___     ___    ___   __  __ /\_\    ___ ___    ]],
    -- [[ / _ `\  / __`\ / __`\/\ \/\ \\/\ \  / __` __`\  ]],
    -- [[/\ \/\ \/\  __//\ \_\ \ \ \_/ |\ \ \/\ \/\ \/\ \ ]],
    -- [[\ \_\ \_\ \____\ \____/\ \___/  \ \_\ \_\ \_\ \_\]],
    -- [[ \/_/\/_/\/____/\/___/  \/__/    \/_/\/_/\/_/\/_/]],
  },
  opts = {
    hl = 'Type',
    position = 'center',
  },
}

local section_mru = {
  type = "group",
  val = {
    {
      type = "text",
      val = "Recent files",
      opts = {
        hl = "SpecialComment",
        shrink_margin = false,
        position = "center",
      },
    },
    { type = "padding", val = 1 },
    {
      type = "group",
      val = function()
        return { mru(0, vim.fn.getcwd()) }
      end,
      opts = { shrink_margin = false },
    },
  },
}

local buttons = {
  type = "group",
  val = {
    { type = "text", val = "Quick links", opts = { hl = "SpecialComment", position = "center" } },
    { type = "padding", val = 1 },
    dashboard.button("e", "  New file", "<cmd>ene<CR>"),
    dashboard.button("f", "  Find file", "<cmd>Telescope find_files<CR>"),
    dashboard.button("s", "  Live grep"),
    dashboard.button("b", "  Browse files", "<cmd>Telescope file_browser<CR>"),
    dashboard.button("c", "  Configuration", "<cmd>e ~/.config/nvim/init.lua <CR>"),
    dashboard.button("u", "  Update plugins", "<cmd>PackerSync<CR>"),
    dashboard.button("t", "  Update TS parsers", "<cmd>TSUpdate<CR>"),
    dashboard.button("q", "  Quit", "<cmd>qa<CR>"),
  },
  position = "center",
}

local info = {
  type = 'text',
  val = info_value(),
  opts = {
    hl = 'SpecialComment',
    position = 'center',
  },
}

local config = {
  layout = {
    { type = 'padding', val = 1 },
    logo,
    { type = 'padding', val = 2 },
    section_mru,
    { type = 'padding', val = 2 },
    buttons,
    { type = 'padding', val = 1 },
    info,
  },
  opts = {
    setup = function()
      vim.api.nvim_create_autocmd('User', {
        pattern = 'AlphaReady',
        desc = 'disable status, tabline and cmdline for alpha',
        callback = function()
          vim.opt.cmdheight = 0
        end,
      })
      vim.api.nvim_create_autocmd('BufUnload', {
        buffer = 0,
        desc = 'enable status, tabline and cmdline after alpha',
        callback = function()
          vim.opt.cmdheight = 1
        end,
      })
    end,
    margin = 5,
  },
}

alpha.setup(config)
