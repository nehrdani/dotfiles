local status, mason = pcall(require, 'mason')
if not status then return end
local lspconfig = require('mason-lspconfig')

mason.setup {}

lspconfig.setup {
  automatic_installation = true,
}
