local status, telescope = pcall(require, 'telescope')
if not status then return end

local actions = require('telescope.actions')
local builtin = require('telescope.builtin')

local function telescope_buffer_dir()
  return vim.fn.expand('%:p:h')
end

local fb_actions = require('telescope').extensions.file_browser.actions

telescope.setup {
  defaults = {
    mappings = {
      i = {
        ['<C-k>'] = actions.move_selection_previous,
        ['<C-j>'] = actions.move_selection_next,
      },
      n = {
        ['q'] = actions.close,
        ['/'] = function()
          vim.cmd('startinsert')
        end,
      },
    },
  },
  pickers = {
    find_files = { hidden = true },
    buffers = { sort_lastused = true },
  },
  extensions = {
    fzf = {
      fuzzy = true, -- false will only do exact matching
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = 'smart_case', -- or "ignore_case" or "respect_case"
    },
    file_browser = {
      theme = 'dropdown',
      mappings = {
        n = {
          ['l'] = actions.select_default,
          ['h'] = fb_actions.goto_parent_dir,
        },
      },
      hijack_netrw = true,
      path = '%:p:h',
      cwd = telescope_buffer_dir(),
      respect_gitignore = false,
      hidden = true,
      grouped = true,
      previewer = false,
      initial_mode = 'normal',
      layout_config = { height = 0.9 }
    },
  },
}

telescope.load_extension("fzf")
telescope.load_extension("file_browser")

vim.keymap.set('n', '<C-p>', builtin.git_files, { desc = '[C-p] Find git files' })
vim.keymap.set('n', '<leader>?', builtin.oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader><space>', builtin.buffers, { desc = '[ ] Find existing buffers' })
vim.keymap.set('n', '<leader>/', function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
    winblend = 10,
    previewer = false,
  })
end, { desc = '[/] Fuzzily search in current buffer]' })
vim.keymap.set('n', '<leader>f', builtin.find_files, { desc = 'Search [F]iles' })
vim.keymap.set('n', '<leader>h', builtin.help_tags, { desc = 'Search [H]elp ' })
vim.keymap.set('n', '<leader>g', builtin.live_grep, { desc = 'Search by [G]rep' })
vim.keymap.set('n', '<leader>w', builtin.grep_string, { desc = 'Search current [W]ord' })
vim.keymap.set('n', '<leader>d', builtin.diagnostics, { desc = 'Search [D]iagnostics' })
vim.keymap.set("n", "<leader>b", telescope.extensions.file_browser.file_browser, { desc = '[B]rowse files' })
