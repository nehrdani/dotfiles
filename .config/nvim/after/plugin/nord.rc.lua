local status, nord = pcall(require, 'nord')
if not status then return end

vim.g.nord_contrast = true
vim.g.nord_uniform_diff_background = false

nord.set()

vim.cmd [[colorscheme nord]]

local colors = require('nord.colors')

vim.api.nvim_set_hl(0, 'DiffAdd', { bg = '#576258' })
vim.api.nvim_set_hl(0, 'DiffChange', { bg = '#3E4D64' })
vim.api.nvim_set_hl(0, 'DiffDelete', { bg = '#5F454E', fg = '#4C566A' })
vim.api.nvim_set_hl(0, 'DiffText', { bg = '#465A76' })
