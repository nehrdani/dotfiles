local status, fidget = pcall(require, 'fidget')
if not status then return end

fidget.setup {
  text = {
    spinner = 'dots_negative'
  },
  window = {
    blend = 0, -- &winblend for the window
  },
  sources = {
    ['null-ls'] = { ignore = true },
  },
}

vim.cmd [[
  augroup fidget-highlights
    autocmd!
    autocmd ColorScheme * highlight FidgetTitle guifg=#81A1C1 guibg=#2E3440 gui=none
    autocmd ColorScheme * highlight FidgetTask guifg=#616E88 guibg=#2E3440 gui=italic
  augroup END
]]
